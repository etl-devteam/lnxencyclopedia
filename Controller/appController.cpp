#include "appController.h"
#include <QDebug>


AppController::AppController(AppView* view, AppModel* model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
}


void AppController::connections()
{
    connect((AppModel*)model, SIGNAL(configureMenuWindow(QString, QStringList*, int)),      (AppView*)view,   SLOT(configureMenuWindow(QString, QStringList*, int)),      static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));
    connect((AppModel*)model, SIGNAL(configureLinesPage (QString, QStringList*, int, int)), (AppView*)view,   SLOT(configureLinesPage (QString, QStringList*, int, int)), static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));
    connect((AppModel*)model, SIGNAL(configureImgPage   (QString, QString,      int, int)), (AppView*)view,   SLOT(configureImgPage   (QString, QString,      int, int)), static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));
    connect((AppModel*)model, SIGNAL(menuPageNextOption()),                                 (AppView*)view,   SLOT(menuPageNextOption()),                                 static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect((AppModel*)model, SIGNAL(menuPagePreviousOption()),                             (AppView*)view,   SLOT(menuPagePreviousOption()),                             static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect((AppModel*)model, SIGNAL(confirmMenuSelection()),                               (AppView*)view,   SLOT(confirmMenuSelection()),                               static_cast<Qt::ConnectionType>(Qt::QueuedConnection));

    connect((AppView*)view,   SIGNAL(openMenuSelection(QString)),                           (AppModel*)model, SLOT(openMenuSelection(QString)),                           static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
}


void AppController::endActions()
{
}
