#ifndef EMPTYMENU_H
#define EMPTYMENU_H

#include "tutorial.h"

class EmptyMenu : public Tutorial
{
    Q_OBJECT

    public:
        EmptyMenu(QString title, Tutorial *parent = nullptr) :
            Tutorial(title, parent)
        {
        }

        QStringList* read() override { return nullptr; }
};


#endif // EMPTYMENU_H
