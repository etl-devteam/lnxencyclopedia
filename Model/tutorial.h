#ifndef TUTORIAL_H
#define TUTORIAL_H

#include <QObject>

enum class TutorialNodesType { menu, manual, emptyMenu };

class Tutorial : public QObject
{
    Q_OBJECT

    public:
        Tutorial(QString title, Tutorial* parent = nullptr) :
            parent(parent),
            title(title),
            type (TutorialNodesType::emptyMenu)
        {
        }

        virtual ~Tutorial() {}


        QString getTitle() const
        {
            return title;
        }


        TutorialNodesType getType() const
        {
            return type;
        }


        virtual QStringList *read() = 0;


        Tutorial* getParent() const
        {
            return parent;
        }


    protected:
        Tutorial* parent;
        QString title;
        TutorialNodesType type;
};


#endif // TUTORIAL_H
