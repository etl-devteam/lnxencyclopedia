#include "manual.h"
#include <QtXml/QDomDocument>
#include <QFile>


Manual::Manual(QString title, QString xmlFilePath, QString manualsPath, Tutorial *parent) :
    Tutorial(title, parent),
    xmlFilePath(xmlFilePath),
    manualsPath(manualsPath),
    indexPages(0),
    copiedInMemory(false)
{
    type=TutorialNodesType::manual;
}


Manual::~Manual()
{
    if (copiedInMemory) removeManualFromMemory();
}


void Manual::removeManualFromMemory()
{
    for (Page *page : pages)
    {
        delete page;
    }

    while (!pages.isEmpty())
    {
        pages.pop_back();
    }

    copiedInMemory = false;
    indexPages = 0;
}


void Manual::copyManualInMemory()
{
    QFile file(xmlFilePath);

    if (!file.open(QIODevice::ReadOnly)) return;

    QDomDocument xmlOM_manual;
    xmlOM_manual.setContent(&file);
    file.close();

    QDomElement root = xmlOM_manual.documentElement();

    if (root.tagName() == "manual")  // Controllo che la root corrisponda a quella della sintassi
    {
        QDomNodeList childrenPages = root.childNodes();

        for (int i = 0; i < childrenPages.count(); i++)  // per ogni pagina
        {
            QDomElement xmlPage = childrenPages.item(i).toElement();
            QStringList lines;
            QString img_path;

            if (xmlPage.tagName() == "page")
            {
                QDomNodeList childrenLines = xmlPage.childNodes();

                for (int i = 0; i < childrenLines.count(); i++)  // per ogni riga/immagine nella pagina
                {
                    QDomElement pageElement = childrenLines.item(i).toElement();

                    if (pageElement.tagName() == "pline")
                    {
                         lines.append("\xe2\x80\xa2" + pageElement.text());
                    }
                    else if (pageElement.tagName() == "line")
                    {
                         lines.append(" " + pageElement.text());
                    }
                    else if (pageElement.tagName() == "image")
                    {
                        if (lines.size() == 0)
                        {  //una pagina con immagine non ha scritte...
                            img_path = pageElement.attribute("src");
                        }
                    }
                }  // fine per ogni riga
            }

            if (lines.size() == 0)
            {
                pages.append(new Page(manualsPath + img_path, pages.size() + 1));
            }
            else
            {
                pages.append(new Page(lines, pages.size() + 1));
            }
        } // fine per ogni pagina
    }

    copiedInMemory = true;
}


QStringList* Manual::read()
{
    if (!copiedInMemory)
    {
        copyManualInMemory();
    }

    // In caso il file xml del manuale non esista la
    // variabile copiedInMemory rimane a false
    if (copiedInMemory)
    {
        switch (currentPageType())
        {
            case PageType::imagePage: return &pages.value(indexPages)->img_path;
            case PageType::linesPage: return &pages.value(indexPages)->lines;
        }
    }

    return nullptr;
}


QStringList* Manual::nextPage()
{
    if (indexPages < pages.size() - 1)
    {
        indexPages++;
        return read();
    }

    return nullptr;
}


QStringList* Manual::previousPage()
{
    if (indexPages > 0)
    {
        indexPages--;
        return read();
    }

    return nullptr;
}

