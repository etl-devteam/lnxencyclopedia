#ifndef MANUAL_H
#define MANUAL_H

#include "tutorial.h"

class Manual : public Tutorial
{
    Q_OBJECT

    public:
        Manual(QString title, QString xmlFilePath, QString manualsPath, Tutorial *parent = nullptr);
        ~Manual();

        QStringList* read() override;

        QStringList* nextPage();
        QStringList* previousPage();
        void removeManualFromMemory();

        enum class PageType {imagePage, linesPage};
        PageType currentPageType() const
        {
            return pages.value(indexPages)->type;
        }

        int currentPageNum() const
        {
            return indexPages + 1;
        }

        int numPages() const
        {
            return pages.size();
        }

        struct Page
        {
            Page(QStringList lines, int num_page) :
                lines(lines),
                num_page(num_page),
                type(PageType::linesPage)
            {
            }

            Page(QString img_path_string, int num_page) :
                img_path {img_path_string},
                num_page(num_page),
                type (PageType::imagePage)
            {
            }


            QStringList lines;
            QStringList img_path;
            int num_page;
            PageType type;
        };


    private:
        void copyManualInMemory();

        QString xmlFilePath;
        const QString manualsPath;

        QList<Page*> pages;
        int indexPages;
        bool copiedInMemory;

    signals:
        void showLinesManual();
        void showImgManual();
};

#endif // MANUAL_H
