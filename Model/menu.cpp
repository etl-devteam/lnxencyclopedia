#include "menu.h"
#include <string.h>


Menu::Menu(QString title, Tutorial *parent) :
    Tutorial(title, parent),
    readOutput(nullptr)
{
    type=TutorialNodesType::menu;
}


Menu::~Menu()
{
    for (Tutorial* tutorial : manualsCollection)
    {
        delete tutorial;
    }

    delete readOutput;
}


void Menu::addManual(Tutorial* new_tutorial)
{
    manualsCollection.append(new_tutorial);
}


QStringList *Menu::read()
{
    if (readOutput != nullptr)
    {
        delete readOutput;
    }

    readOutput = new QStringList;

    for (Tutorial* man : manualsCollection)
    {
       readOutput->append(man->getTitle());
    }

    return readOutput;
}


Tutorial* Menu::getOption(QString title) const
{
    for (auto* value : manualsCollection)
    {
        if (value->getTitle() == title) return value;
    }

    return manualsCollection.back();
}


int Menu::getOptionNum(QString opt_name) const
{
    for (int i = 0; i < manualsCollection.size(); i++)
    {
        if (manualsCollection.value(i)->getTitle() == opt_name)
        {
            return i;
        }
    }

    return -1;
}
