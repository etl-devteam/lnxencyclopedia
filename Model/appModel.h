#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>
#include <QtXml/QDomDocument>
#include "tutorial.h"


class AppModel : public AbstractVisionARModel
{
    Q_OBJECT

    public:
        AppModel(QString pathXMLFiles);
        ~AppModel() override;

    private:
        Tutorial* tutorialTree;
        QString optionSelected;
        const QString pathXMLFiles;

        enum class AppStatus {WELCOME, MENU_MODE, READING_MODE};
        AppStatus status;

        void runModel() override;

        void backPressed() override {}
        void backReleased(int timePressed) override;

        void okPressed() override;
        void okReleased(int /*timePressed*/ = 0) override {}

        void forwardPressed() override;
        void forwardReleased(int /*timePressed*/) override {}

        void backwardPressed() override;
        void backwardReleased(int /*timePressed*/) override {}

        void singleTap() override {}
        void doubleTap() override {}

        void swipeForward() override {}
        void swipeBackward() override {}

        void manageAlertsEnd(QString id, bool value) override;

        void generateTree(QDomElement xmlTreeRoot, Tutorial* treeRoot);

    public slots:
        void openMenuSelection(QString title);

    signals:
        void configureMenuWindow(QString, QStringList*, int);
        void configureLinesPage(QString, QStringList*, int, int);
        void configureImgPage(QString, QString, int, int);

        void menuPageNextOption();
        void menuPagePreviousOption();
        void confirmMenuSelection();
};

#endif // APPMODEL_H
