﻿#include "appModel.h"
#include "menu.h"
#include "manual.h"
#include "emptyMenu.h"
#include <QFile>
#include <QDebug>


AppModel::AppModel(QString pathXMLFiles) :
   AbstractVisionARModel(),
   tutorialTree(new Menu("Menu Principale")),
   pathXMLFiles(pathXMLFiles),
   status(AppStatus::WELCOME)
{
}


AppModel::~AppModel()
{
    delete tutorialTree;
}


void AppModel::generateTree(QDomElement xmlTreeRoot, Tutorial* treeRoot)
{
    QDomNodeList children = xmlTreeRoot.childNodes();

    for (int i = 0; i < children.count(); i++)  // per ogni figlio
    {
        Tutorial *newChild = nullptr;
        QDomElement xmlChild = children.item(i).toElement();

        if (xmlChild.tagName() == "menu")
        {
            if (xmlChild.childNodes().count() == 0)
            {
                newChild = new EmptyMenu(xmlChild.attribute("title"), treeRoot);
            }
            else
            {
                newChild = new Menu(xmlChild.attribute("title"), treeRoot);
                generateTree(xmlChild, newChild);
            }
        }
        else if (xmlChild.tagName() == "manual")
        {
            newChild = new Manual(xmlChild.attribute("title"), pathXMLFiles + xmlChild.attribute("src"),
                                  pathXMLFiles, treeRoot);
        }

        ((Menu*)treeRoot)->addManual(newChild);
    }
}


void AppModel::runModel()
{
    QFile file (pathXMLFiles + "../tutorial_structure.xml");
    QDomDocument xmlOM_structure;

    if (file.open(QIODevice::ReadOnly))
    {
        xmlOM_structure.setContent(&file);
        file.close();

        QDomElement root = xmlOM_structure.documentElement();

        if (root.tagName() == "tutorial")
        {
            generateTree(root, tutorialTree);
        }

        emit showWindow(1);
    }
    else
    {
        emit newAlert(2, "no_structure_xml", "tutorial_structure.xml not found.");
    }
}


void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
        case AppStatus::WELCOME:
            emit newAlert(1, "QuestionCloseApp", "Do you want to return to the Launcher?");
            break;

        case AppStatus::MENU_MODE:
            if (tutorialTree->getParent() == nullptr)
            {
                emit showWindow(1);
                status = AppStatus::WELCOME;
            }
            else
            {
                // Uso il titolo della pagina che sto togliendo per recuperare
                // il numero dell'opzione in cui si è entrati nel menu padre
                const QString oldTitle = tutorialTree->getTitle();

                tutorialTree = tutorialTree->getParent(); // di sicuro si che siamo in una foglia che in un branch in parent è un branch

                int optionFather = ((Menu*)tutorialTree)->getOptionNum(oldTitle);

                QStringList* nodeData = tutorialTree->read();

                emit configureMenuWindow(tutorialTree->getTitle(), nodeData, optionFather);
                emit showWindow(2);

                status = AppStatus::MENU_MODE;
            }

            break;

        case AppStatus::READING_MODE:
            {
                const QString oldTitle = tutorialTree->getTitle();

                ((Manual*)tutorialTree)->removeManualFromMemory();
                tutorialTree = tutorialTree->getParent(); // di sicuro si che siamo in una foglia che in un branch in parent è un branch

                const int optionFather = ((Menu*)tutorialTree)->getOptionNum(oldTitle);
                QStringList* nodeData = tutorialTree->read();

                emit configureMenuWindow(tutorialTree->getTitle(), nodeData, optionFather);
                emit showWindow(2);

                status = AppStatus::MENU_MODE;
            }

            break;
    }
}


void AppModel::forwardPressed()
{
    switch(status)
    {
        case AppStatus::WELCOME:
            break;

        case AppStatus::MENU_MODE:
            emit menuPagePreviousOption();
            break;

        case AppStatus::READING_MODE:
            QStringList* prevLines = ((Manual*)tutorialTree)->previousPage();

            // Se non si può più andare indietro con le pagine previousPage
            // ritorna nullptr
            if (prevLines != nullptr)
            {
                if (((Manual*)tutorialTree)->currentPageType() == Manual::PageType::linesPage)
                {
                    emit configureLinesPage(tutorialTree->getTitle(), prevLines, ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages() );
                    emit showWindow(4);
                }
                else
                {
                    emit configureImgPage(tutorialTree->getTitle(), prevLines->value(0), ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                    emit showWindow(3);
                }
            }

            break;
    }
}


void AppModel::backwardPressed()
{
    switch(status)
    {
        case AppStatus::WELCOME:
            break;

        case AppStatus::MENU_MODE:
            emit menuPageNextOption();
            break;

        case AppStatus::READING_MODE:

            QStringList* nextLines = ((Manual*)tutorialTree)->nextPage();

            // Se non si può più andare avanti con le pagine nextPage
            // ritorna nullptr
            if (nextLines != nullptr)
            {
                if (((Manual*)tutorialTree)->currentPageType() == Manual::PageType::linesPage)
                {
                    emit configureLinesPage(tutorialTree->getTitle(), nextLines, ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                    emit showWindow(4);
                }
                else
                {
                    emit configureImgPage(tutorialTree->getTitle(), nextLines->value(0), ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                    emit showWindow(3);
                }
            }

            break;
    }
}


void AppModel::okPressed()
{
    switch (status)
    {
        case AppStatus::WELCOME:
            {
                QStringList* nodeData = tutorialTree->read();

                switch (tutorialTree->getType())
                {
                    case TutorialNodesType::manual:
                        {
                            if (((Manual*)tutorialTree)->currentPageType() == Manual::PageType::linesPage)
                            {
                                emit configureLinesPage(tutorialTree->getTitle(), nodeData, ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                                emit showWindow(4);
                            }
                            else
                            {
                                emit configureImgPage(tutorialTree->getTitle(), nodeData->value(0), ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                                emit showWindow(3);
                            }

                            status = AppStatus::READING_MODE;
                        }

                        break;

                    case TutorialNodesType::menu:
                        {
                            emit configureMenuWindow(tutorialTree->getTitle(), nodeData, 0);
                            emit showWindow(2);

                            status = AppStatus::MENU_MODE;
                        }

                        break;

                    default: break;
                }
            }

            break;

        case AppStatus::MENU_MODE:
            emit confirmMenuSelection();
            break;

        case AppStatus::READING_MODE:
            break;
    }
}


void AppModel::manageAlertsEnd(QString id, bool value)
{
    switch (status)
    {
        case AppStatus::WELCOME:
            if (id == "QuestionCloseApp")
            {
                if (value)
                {
                     emit endApplication();
                }
            }
            else if (id == "no_structure_xml")
            {
                emit endApplication();
            }

            break;

        case AppStatus::MENU_MODE:
            if (id == "no_menu_children")
            {
                status = AppStatus::MENU_MODE;
            }
            else if (id == "no_manual_xml")
            {
                status = AppStatus::MENU_MODE;
            }

            break;

        case AppStatus::READING_MODE: break;
    }
}


void AppModel::openMenuSelection (QString title)
{
    tutorialTree = ((Menu*)tutorialTree)->getOption(title);

    QStringList* nodeData = tutorialTree->read();

    if (nodeData == nullptr)
    {
        emit newAlert(2, "no_manual_xml", "Manual not found.");
        return;
    }

    switch (tutorialTree->getType())
    {
        case TutorialNodesType::manual:
            if (((Manual*)tutorialTree)->currentPageType() == Manual::PageType::linesPage)
            {
                emit configureLinesPage(tutorialTree->getTitle(), nodeData, ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                emit showWindow(4);
            }
            else
            {
                emit configureImgPage(tutorialTree->getTitle(), nodeData->value(0), ((Manual*) tutorialTree)->currentPageNum(), ((Manual*) tutorialTree)->numPages());
                emit showWindow(3);
            }

            status = AppStatus::READING_MODE;
            break;

        case TutorialNodesType::menu:
            emit configureMenuWindow(tutorialTree->getTitle(), nodeData, 0);
            emit showWindow(2);

            status = AppStatus::MENU_MODE;
            break;

        case TutorialNodesType::emptyMenu:
            emit newAlert(2, "no_menu_children", "Empty menu.");
            break;
    }
}

