#ifndef MENU_H
#define MENU_H

#include "tutorial.h"

class Menu : public Tutorial
{
    Q_OBJECT

    public:
        Menu(QString title, Tutorial *parent = nullptr);
        ~Menu();

        void addManual(Tutorial* new_tutorial);
        QStringList* read() override;

        Tutorial* getOption(QString title) const;
        int getOptionNum(QString opt_name) const;  // ritorna il numero dell'opzione nella lista dato il suo nome

    private:
        QList<Tutorial*> manualsCollection;

        QStringList *readOutput;

    signals:
        void showMenu(QStringList *);
};

#endif // MENU_H
