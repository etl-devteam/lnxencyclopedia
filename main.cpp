#include <QApplication>
#include <QDebug>

#include "Controller/appController.h"
#include "Model/appModel.h"
#include "View/appView.h"


bool validateParameters(int argc, char *argv[]);


int main(int argc, char *argv[])
{
    if (validateParameters(argc, argv) || argc == 1 /* use default parameters */)
    {
        const QString mainTitle           = argc == 1 ? "Il mondo animale" : argv[1];
        const QString manualsAbsolutePath = argc == 1 ? "/etc/VisionAR/QtDemoAnimalsApp/Manuals/" : argv[2];
        const QString mainIcon            = argc == 4 ? argv[3] :
                (argc == 1 ? "/etc/VisionAR/QtDemoAnimalsApp/welcomeIcon.jpg" : "");

        QApplication a(argc, argv);

        AppController::startCheckingGlasses();
        AppController controller(new AppView(mainTitle, mainIcon), new AppModel(manualsAbsolutePath), "QtDemoAnimalsApp/QtDemoAnimalsApp");

        return a.exec();
    }

    return -1;
}


bool validateParameters(int argc, char *argv[])
{
    if (argc == 3 || argc == 4)
    {
        if (QString(argv[2]).endsWith('/'))
        {
            return true;
        }

        qDebug() << "Error: absolute path must be a directory\n";
    }
    else if (argc != 1 /* for default parameters dont print error message */)
    {
        qDebug() << "Usage:\n"
                    "$ <welcome page title> <manuals absolute path> <optional exe icon>\n";
    }

    return false;
}
