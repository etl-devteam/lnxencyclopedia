#include "linesManualView.h"


LinesManualView::LinesManualView(QString coloreVisibile) :
    ViewPagesInterface(coloreVisibile),
    ui_page(new Ui::linesPage)
{
}


void LinesManualView::setupUi(QWidget* widget)
{
    widgetToConnect=widget;
    ui_page->setupUi(widget);
    ui_page->lines->setStyleSheet("font: 13px \"Courier\"; font-weight: bold;");
    ui_page->page_num->setStyleSheet("font: 10px; font-weight: bold;");
    ui_page->title->setFont(title_font);
}


void LinesManualView::set(QString title, QStringList* options, int page_num, int num_pages)
{
    this->title=title;

    while (!rows.isEmpty())
    {
        rows.pop_back();
    }

    for (QString opt : *options)
    {
        rows.append(opt);
    }

    ui_page->title->setText(title);
    configureTitleLabel(ui_page->title);

    ui_page->lines->setText("");
    for (int i = 0; i < 4; i++)
    {
        if (rows.size() > i)
        {
            ui_page->lines->setText(ui_page->lines->text()+"\n"+rows.value(i));
        }
    }

    ui_page->page_num->setText("Pag. "+QString::number(page_num)+" di "+QString::number(num_pages));
}
