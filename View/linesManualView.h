#ifndef LINESMANUALVIEW_H
#define LINESMANUALVIEW_H

#include "ui_page_with_lines.h"
#include "viewPagesInterface.h"

class LinesManualView : public ViewPagesInterface
{
    public:
        LinesManualView(QString coloreVisibile);

        void set(QString title, QStringList* options, int page_num, int num_pages) override;
        void setupUi(QWidget*);

    private:
        Ui::linesPage* ui_page;
        QStringList rows;

};

#endif // LINESMANUALVIEW_H
