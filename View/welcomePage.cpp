#include "welcomePage.h"


WelcomePage::WelcomePage(QString title, QString welcomeIcon):
    welcome_page(new Ui_Welcome_page),
    mainTitle(title),
    welcomeIcon(welcomeIcon)
{
}


void WelcomePage::setupUi(QWidget* widget)
{
    welcome_page->setupUi(widget);

    welcome_page->title->setText(mainTitle);

    QFont title_font;
    QFont enter_font;

    title_font.setPixelSize(14);

    enter_font.setBold(true);
    enter_font.setFamily("Courier");
    enter_font.setPixelSize(12);

    welcome_page->title->setFont(title_font);
    welcome_page->label_enter->setFont(enter_font);

    if (!welcomeIcon.isEmpty())
    {
        welcome_page->optional_icon->setPixmap(QPixmap(welcomeIcon).scaled(welcome_page->optional_icon->size(),
                                                                           Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
}
