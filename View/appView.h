#ifndef APPVIEW_H
#define APPVIEW_H

#include <QScopedPointer>
#include <VisionAR/Framework/abstractVisionARView.h>

#include "welcomePage.h"
#include "menuView.h"
#include "linesManualView.h"
#include "imgManualView.h"


class AppView : public AbstractVisionARView
{
    Q_OBJECT

    public:
        AppView(QString mainTitle, QString welcomeIcon);
        ~AppView() = default;

    private:
        QScopedPointer<WelcomePage>     welcome_page;
        QScopedPointer<MenuView>        menuPage;
        QScopedPointer<LinesManualView> lines_page;
        QScopedPointer<ImgManualView>   image_page;

    public slots:
        void configureMenuWindow(QString, QStringList*, int option_num);
        void configureLinesPage(QString, QStringList*, int, int);
        void configureImgPage(QString, QString, int, int);

        void confirmMenuSelection();
        void menuPageNextOption();
        void menuPagePreviousOption();

    signals:
        void openMenuSelection(QString title);
};

#endif // APPVIEW_H
