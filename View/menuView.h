#ifndef MENUVIEW_H
#define MENUVIEW_H

#include "ui_menu.h"
#include "viewPagesInterface.h"
#include "blinker.h"
#include "labelMover.h"


class MenuView : public ViewPagesInterface
{
    public:
        MenuView(QString coloreVisibile);
        ~MenuView();

        void set(QString title, QStringList* options, int option_num=0) override;
        void nextOption();
        void previousOption();
        QString getSelection(); // ritorna il titolo della selezione considerato univoco dentro lo
                                // stesso menu
        void stopMotion();

        void setupUi(QWidget*);

    private:
        Ui_menu* menu_page;
        QStringList optionsList;

        int index_current_tutorial; // quale opzione stiamo puntando in optionList
        enum class LabelSelected { label1, label2, label3 };
        LabelSelected index_in_ui; // quale delle tre label visibili è selezionata

        QPixmap arrow_up;
        QPixmap arrow_down;

        Blinker *blinker;
        LabelMover labelMover;

        const QString styleSelectedLabel;
        const QString styleUnselectedLabel;

        void detectLabelSelection();
        void setMenuLabels(QString optSelected, QString opt1, QString opt2);
};


#endif // MENUVIEW_H
