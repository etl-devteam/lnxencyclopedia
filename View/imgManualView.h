#ifndef IMGMANUALVIEW_H
#define IMGMANUALVIEW_H

#include "ui_page_with_image.h"
#include "viewPagesInterface.h"
class ImgManualView : public ViewPagesInterface
{
    public:
        ImgManualView(QString coloreVisibile);

        void set(QString title, QString image_path, int page_num, int num_pages) override;
        void setupUi(QWidget*);

     private:
        Ui_imagePage* image_page;
        QString image_path;
        QPixmap img;
};

#endif // IMGMANUALVIEW_H
