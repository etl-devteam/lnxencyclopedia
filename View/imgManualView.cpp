#include "imgManualView.h"


ImgManualView::ImgManualView(QString coloreVisibile) :
    ViewPagesInterface(coloreVisibile),
    image_page(new Ui::imagePage)
{
}


void ImgManualView::setupUi(QWidget* widget)
{
    widgetToConnect=widget;
    image_page->setupUi(widget);
    image_page->page_num->setStyleSheet("font: 10px; font-weight: bold;");
    image_page->title->setFont(title_font);
}


void ImgManualView::set(QString title, QString image_path, int page_num, int num_pages)
{
    this->title=title;
    this->image_path=image_path;

    img.load(image_path);
    image_page->title->setText(title);
    configureTitleLabel(image_page->title);

    image_page->image->setPixmap(img.scaled(image_page->image->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
   // image_page->image->setStyleSheet("border: 1px solid "+coloreVisibile+";");
    image_page->page_num->setText("Pag. "+QString::number(page_num)+" di "+QString::number(num_pages));
}
