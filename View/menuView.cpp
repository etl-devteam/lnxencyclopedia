#include "menuView.h"
#include <QDebug>


MenuView::MenuView(QString coloreVisibile) :
    ViewPagesInterface(coloreVisibile),
    menu_page(new Ui::menu),
    index_current_tutorial(0),
    arrow_up(":/Arrows/Icons/arrow_up.png"),
    arrow_down(":/Arrows/Icons/arrow_down.png"),
    labelMover(MAX_CHARS_PER_LABEL),
    styleSelectedLabel("font: 18px \"Courier\"; border: 1px solid " + coloreVisibile + ";"),
    styleUnselectedLabel("font: 12px \"Courier\"; border: 0px;")
{
}


MenuView::~MenuView()
{
    delete blinker;
}


void MenuView::setupUi(QWidget* widget)
{
    widgetToConnect = widget;
    menu_page->setupUi(widget);

    menu_page->title->setFont(title_font);
    menu_page->up_icon->setPixmap(arrow_up.scaled(menu_page->up_icon->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    menu_page->down_icon->setPixmap(arrow_down.scaled(menu_page->down_icon->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));

    blinker = new Blinker(menu_page->up_icon, menu_page->down_icon);
    blinker->startBlink(true, false);
}


void MenuView::stopMotion()
{
    labelMover.stopMoving();
}


void MenuView::set(QString title, QStringList* options, int option_num)
{
    this->title=title;

    labelMover.stopMoving();
    while (!optionsList.isEmpty())
    {
        optionsList.pop_back();
    }

    for (QString option : *options )
    {
        optionsList.append(option);
    }

    menu_page->title->setText(title);
    configureTitleLabel(menu_page->title);

    index_current_tutorial = option_num;
    detectLabelSelection();
}


void MenuView::detectLabelSelection()
{
    QString opt1;
    QString opt2;

    switch (optionsList.size())
    {
        case 1: //Il menu ha solo un'opzione
            {
                index_in_ui =LabelSelected::label1;
                menu_page->option_1->setText(optionsList.value(0));
                menu_page->option_2->setText("");
                menu_page->option_3->setText("");

                menu_page->option_1->setStyleSheet(styleSelectedLabel);
                menu_page->option_2->setStyleSheet(styleUnselectedLabel);
                menu_page->option_3->setStyleSheet(styleUnselectedLabel);

                blinker->cursorAtBottom=true;
                blinker->cursorAtTop=true;
                labelMover.setLabel(menu_page->option_1);
            }

            break;

        case 2: //il menu ha due opzioni
            {
                menu_page->option_1->setText(optionsList.value(0));
                menu_page->option_2->setText(optionsList.value(1));
                menu_page->option_3->setText("");

                if (index_current_tutorial == 0)
                {
                    index_in_ui =LabelSelected::label1;
                    menu_page->option_1->setStyleSheet(styleSelectedLabel);
                    menu_page->option_2->setStyleSheet(styleUnselectedLabel);
                    menu_page->option_3->setStyleSheet(styleUnselectedLabel);

                    blinker->cursorAtBottom=false;
                    blinker->cursorAtTop=true;

                    labelMover.setLabel(menu_page->option_1);

                }
                else if (index_current_tutorial == 1)
                {
                    index_in_ui =LabelSelected::label2;
                    menu_page->option_1->setStyleSheet(styleUnselectedLabel);
                    menu_page->option_2->setStyleSheet(styleSelectedLabel);
                    menu_page->option_3->setStyleSheet(styleUnselectedLabel);

                    blinker->cursorAtBottom=true;
                    blinker->cursorAtTop=false;

                    labelMover.setLabel(menu_page->option_2);
                }
            }

            break;

        default: //il menu ha n opzioni
            {
                if (index_current_tutorial == 0)
                {
                    index_in_ui =LabelSelected::label1;
                    opt1 = optionsList.value(index_current_tutorial+1);
                    opt2 = optionsList.value(index_current_tutorial+2);
                }
                else if (index_current_tutorial == optionsList.size()-1)
                {
                    index_in_ui =LabelSelected::label3;
                    opt1 = optionsList.value(index_current_tutorial-2);
                    opt2 = optionsList.value(index_current_tutorial-1);
                }
                else
                {
                    index_in_ui =LabelSelected::label2;
                    opt1 = optionsList.value(index_current_tutorial-1);
                    opt2 = optionsList.value(index_current_tutorial+1);
                }

                setMenuLabels(optionsList.value(index_current_tutorial), opt1, opt2);
            }

            break;
    }
}


void MenuView::setMenuLabels(QString optSelected, QString opt1, QString opt2)
{
    switch(index_in_ui)
    {
        case LabelSelected::label1:

            menu_page->option_1->setText(optSelected);
            menu_page->option_2->setText(opt1);
            menu_page->option_3->setText(opt2);

            menu_page->option_1->setStyleSheet(styleSelectedLabel);
            menu_page->option_2->setStyleSheet(styleUnselectedLabel);
            menu_page->option_3->setStyleSheet(styleUnselectedLabel);

            blinker->cursorAtBottom=false;
            blinker->cursorAtTop=true;

            labelMover.setLabel(menu_page->option_1);

            break;

        case LabelSelected::label2:
            menu_page->option_2->setText(optSelected);
            menu_page->option_1->setText(opt1);
            menu_page->option_3->setText(opt2);

            menu_page->option_1->setStyleSheet(styleUnselectedLabel);
            menu_page->option_2->setStyleSheet(styleSelectedLabel);
            menu_page->option_3->setStyleSheet(styleUnselectedLabel);

            blinker->cursorAtBottom=false;
            blinker->cursorAtTop=false;

            labelMover.setLabel(menu_page->option_2);

            break;

        case LabelSelected::label3:
            menu_page->option_3->setText(optSelected);
            menu_page->option_1->setText(opt1);
            menu_page->option_2->setText(opt2);

            menu_page->option_1->setStyleSheet(styleUnselectedLabel);
            menu_page->option_2->setStyleSheet(styleUnselectedLabel);
            menu_page->option_3->setStyleSheet(styleSelectedLabel);

            blinker->cursorAtBottom=true;
            blinker->cursorAtTop=false;

            labelMover.setLabel(menu_page->option_3);

            break;
    }
}


void MenuView::nextOption()
{
    if (index_current_tutorial < optionsList.size()-1)
    {
        index_current_tutorial++;
        labelMover.stopMoving();

        detectLabelSelection();
    }
    else
    {
        qDebug() << "No next options..." << endl;
    }
}


void MenuView::previousOption()
{
    if (index_current_tutorial > 0)
    {
        index_current_tutorial--;
        labelMover.stopMoving();

        detectLabelSelection();
    }
    else
    {
        qDebug() << "No previous options..." << endl;
    }
}


QString MenuView::getSelection()
{
    return optionsList.value(index_current_tutorial);
}
