#ifndef BLINKER_H
#define BLINKER_H

#include <QObject>
#include <QTimer>
#include <QLabel>

class Blinker : public QObject
{
    Q_OBJECT

    public:
        explicit Blinker(QLabel *arrow_up, QLabel *arrow_down);
        bool cursorAtTop;
        bool cursorAtBottom;

        void startBlink(bool cursorTop, bool cursorBottom);
        void stopBlink();

    private:
        bool blink_on;
        QLabel *arrow_up;
        QLabel *arrow_down;
        QTimer timer_blink;

    private slots:
        void blinkArrows();
};

#endif // BLINKER_H
