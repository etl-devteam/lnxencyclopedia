#include "blinker.h"

const int FREQ_BLINK = 500;


Blinker::Blinker(QLabel *arrow_up, QLabel *arrow_down) :
    QObject(nullptr),
    cursorAtTop(true),
    cursorAtBottom(false),
    blink_on(true),
    arrow_up(arrow_up),
    arrow_down(arrow_down)
{
    connect(&timer_blink, SIGNAL(timeout()), this, SLOT(blinkArrows()));
    timer_blink.setInterval(FREQ_BLINK);
}


void Blinker::startBlink(bool cursorTop, bool cursorBottom)
{
    cursorAtTop = cursorTop;
    cursorAtBottom = cursorBottom;

    timer_blink.start();
}


void Blinker::stopBlink()
{
    timer_blink.stop();
    blink_on=true;
    cursorAtTop=true;
    cursorAtBottom=false;
}


void Blinker::blinkArrows()
{
    if (blink_on)
    {
        if (!cursorAtTop)
        {
            arrow_up->hide();
        }

        if (!cursorAtBottom)
        {
            arrow_down->hide();
        }

        blink_on = false;
    }
    else
    {
        if (!cursorAtTop)
        {
            arrow_up->show();
        }

        if (!cursorAtBottom)
        {
            arrow_down->show();
        }

        blink_on = true;
    }

    if (cursorAtTop)
    {
        arrow_up->hide();
    }
    else if (cursorAtBottom)
    {
        arrow_down->hide();
    }
}
