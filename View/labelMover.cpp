#include "labelMover.h"
#include <QDebug>

const int FREQ_MOVEMENT = 200;
const int DELAY_PRE_MOTION = 1200;

int spacesRemoved = 1;
int counterReshowedChars = 0;
bool motionToStart = false; // per proteggermi da partenze inaspettate di vecchi timer


LabelMover::LabelMover(const int MAX_CHARS_PER_LABEL) :
    QObject(nullptr),
    original_string(""),
    counter(0),
    wordHidden(false),
    MAX_CHARS_PER_LABEL(MAX_CHARS_PER_LABEL)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(moveLabelShortCycle()));
    timer.setInterval(FREQ_MOVEMENT);
}


LabelMover::~LabelMover()
{
    timer.stop();
}


void LabelMover::stopMoving()
{
    if (!original_string.isEmpty())
    {
        while (delayTimer.isActive());
        timer.stop();
        while (timer.isActive());
        this->labelToMove->setText(original_string);
        wordHidden = false;
        counter = 0;
    }
}


void LabelMover::setLabel(QLabel *lab)
{
    labelToMove=lab;
    original_string=lab->text();
    counter = 0;
    wordHidden = false;

    if (labelToMove->text().size() > MAX_CHARS_PER_LABEL)
    { // Muove solo se la scritta è troppo grande
        motionToStart=true;
        delayTimer.singleShot(DELAY_PRE_MOTION, this, SLOT(startMotion()));
    }
    else
    {
         motionToStart=false;
    }
}


void LabelMover::startMotion()
{
    if (motionToStart)
    {
        timer.start();
    }
}


/*
 * Il seguente metodo puo' essere migliorato in quanto la label presenta delle leggere accelerazioni sparse
 */

void LabelMover::moveLabelShortCycle()
{
    QString aux;
    const int NUM_SPACES_AFTER = 6;

    if (counter >= NUM_SPACES_AFTER)  // Se sono sparite 6 lettere ricomincia a far apparire la stringa
    {
        if (counter >= original_string.size() && counter <= original_string.size()+NUM_SPACES_AFTER)
        {
            for (int i =0; i < NUM_SPACES_AFTER-(counter-original_string.size()); i++)
            {
                aux.append(" ");
            }

            aux.append(original_string.mid(0, original_string.size()-counterReshowedChars));

            this->labelToMove->setText(aux);

            counterReshowedChars--;

            if (counter == original_string.size()+NUM_SPACES_AFTER)
            {
                counter=0;
            }
        }
        else
        {
            QString aux=original_string.mid(counter, original_string.size()-counter);

            for (int i =0; i < NUM_SPACES_AFTER; i++)
            {
                aux.append(" ");
            }

            aux.append(original_string.mid(0, original_string.size()-counterReshowedChars));

            this->labelToMove->setText(aux);

            counterReshowedChars--;
        }
    }
    else
    {
        this->labelToMove->setText(original_string.mid(counter, original_string.size()-counter));
        counterReshowedChars =original_string.size()-1;
    }

    counter++;
}


void LabelMover::moveLabelLongCycle()
{
    QString aux;

    if (!wordHidden)
    {
        this->labelToMove->setText(original_string.mid(counter, original_string.size()-counter));

        if (original_string.size()-counter+1 == 0)
        {
            counter =0;
            wordHidden = true;
        }
    }
    else
    {
        for (int i = 0; i < MAX_CHARS_PER_LABEL-counter; i++)
        {
            aux.append(" ");
        }

        aux.append(original_string.mid(0, counter));
        this->labelToMove->setText(aux);

        if (counter+1 == MAX_CHARS_PER_LABEL)
        {
            counter =-1;
            wordHidden=false;
        }
    }

    counter++;
}
