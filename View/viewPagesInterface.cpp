#include "viewPagesInterface.h"


ViewPagesInterface::ViewPagesInterface(QString coloreVisibile):
    coloreVisibile(coloreVisibile)
{
    title_font.setFamily("Courier");
    title_font.setPixelSize(18);
    title_font.setBold(true);

    title_font_metrics = new QFontMetrics(title_font);
}


ViewPagesInterface::~ViewPagesInterface()
{
    delete title_font_metrics;
}


void ViewPagesInterface::configureTitleLabel(QLabel *titleLabel)
{
    if (titleLabel->text().size() > MAX_CHARS_PER_LABEL)
    {
        QString elidedText = title_font_metrics->elidedText(titleLabel->text(), Qt::ElideRight, title_font_metrics->width(titleLabel->text(), MAX_CHARS_PER_LABEL));
        titleLabel->setText(elidedText);
        titleLabel->setAlignment(Qt::AlignLeft);
    }
    else
    {
        titleLabel->setAlignment(Qt::AlignCenter);
    }
}
