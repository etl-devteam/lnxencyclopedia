#ifndef VIEWPAGESINTERFACE_H
#define VIEWPAGESINTERFACE_H

#include <QWidget>
#include <QLabel>
#include <QFontMetrics>

constexpr int MAX_CHARS_PER_LABEL = 36;

class ViewPagesInterface
{
    public:
        ViewPagesInterface(QString coloreVisibile);
        virtual ~ViewPagesInterface();

        virtual void set(QString /*title*/, QStringList* /*options*/, int /*option_num*/)
        {
        }

        virtual void set(QString /*title*/, QStringList* /*options*/, int /*page_num*/, int /*num_pages*/)
        {
        }

        virtual void set(QString /*title*/, QString /*options*/, int /*page_num*/, int /*num_pages*/)
        {
        }

    protected:
        QString title;
        QFont title_font;

        QString coloreVisibile;
        QWidget* widgetToConnect;

        void configureTitleLabel(QLabel *titleLabel); //aggiunge i puntini se la scritta e' troppo lunga

    private:
        QFontMetrics *title_font_metrics;
};


#endif // VIEWPAGESINTERFACE_H
