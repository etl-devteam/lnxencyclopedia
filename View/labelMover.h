#ifndef LABELMOVER_H
#define LABELMOVER_H

#include <QObject>
#include <QLabel>
#include <QTimer>

class LabelMover : public QObject
{
    Q_OBJECT

    public:
        explicit LabelMover(const int MAX_CHARS_PER_LABEL);
        ~LabelMover();

        void setLabel(QLabel *labelToMove);
        void stopMoving();

    private:
        QLabel *labelToMove;
        QString original_string;
        QTimer delayTimer;
        QTimer timer;
        int counter; //conta il numero di caratteri spostati
        bool wordHidden; //quando la parole è scoparsa e sta ricostruendosi da destra è a true
        const int MAX_CHARS_PER_LABEL;

    private slots:
        void moveLabelLongCycle();
        void moveLabelShortCycle();
        void startMotion();
};

#endif // LABELMOVER_H
