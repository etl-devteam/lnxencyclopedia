#ifndef WELCOMEPAGE_H
#define WELCOMEPAGE_H

#include "ui_welcome.h"

class WelcomePage
{
public:
    WelcomePage(QString title, QString welcomeIcon);

    void setupUi(QWidget*);

private:
    Ui_Welcome_page *welcome_page;

    const QString mainTitle, welcomeIcon;
};

#endif // WELCOMEPAGE_H
