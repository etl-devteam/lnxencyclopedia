#include "appView.h"


AppView::AppView(QString mainTitle, QString welcomeIcon) :
    AbstractVisionARView(),
    welcome_page(new WelcomePage(mainTitle, welcomeIcon)),
    menuPage(new MenuView(coloreVisibile)),
    lines_page(new LinesManualView(coloreVisibile)),
    image_page(new ImgManualView(coloreVisibile))
{
    registerWindow(welcome_page.data());
    registerWindow(menuPage.data());
    registerWindow(image_page.data());
    registerWindow(lines_page.data());
}


void AppView::configureMenuWindow(QString title, QStringList* options, int option_num)
{
    menuPage->set(title, options, option_num);
}


void AppView::menuPageNextOption()
{
    menuPage->nextOption();
}


void AppView::menuPagePreviousOption()
{
    menuPage->previousOption();
}


void AppView::confirmMenuSelection()
{
    emit openMenuSelection(menuPage->getSelection());
}


void AppView::configureLinesPage(QString title, QStringList* lines, int page_num, int num_pages)
{
    menuPage->stopMotion();
    lines_page->set(title, lines, page_num, num_pages);
}


void AppView::configureImgPage(QString title, QString img, int page_num, int num_pages)
{
    menuPage->stopMotion();
    image_page->set(title, img, page_num, num_pages);
}
