QT += core widgets gui xml

CONFIG += c++11

TARGET = QtDemoAnimalsApp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
           Model/appModel.cpp \
           Model/manual.cpp \
           Model/menu.cpp \
           Model/emptyMenu.cpp \
           Controller/appController.cpp \
           View/appView.cpp \
           View/menuView.cpp \
           View/linesManualView.cpp \
           View/imgManualView.cpp \
           View/viewPagesInterface.cpp \
           View/blinker.cpp \
           View/labelMover.cpp \
           View/welcomePage.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -lBtnsEventGen2 -lGlassesCheck -lTouchEventsGen2 -lVisionARAlertFactory \
        -lAbstractVisionARController -lAbstractVisionARModel -lAbstractVisionARView \
        -lThreadObjInterface -lBtnsInterface -lBatteryManager -lAlertsManager -lLauncherAdapterInterface -lTouchInterface

HEADERS += Model/appModel.h \
           Model/tutorial.h \
           Model/manual.h \
           Model/menu.h \
           Model/emptyMenu.h \
           View/appView.h \
           View/menuView.h \
           View/viewPagesInterface.h \
           View/linesManualView.h \
           View/imgManualView.h \
           View/blinker.h \
           View/labelMover.h \
           View/welcomePage.h \
           Controller/appController.h

FORMS += View/welcome.ui \
         View/page_with_image.ui \
         View/menu.ui \
         View/page_with_lines.ui

RESOURCES += \
    Images.qrc
